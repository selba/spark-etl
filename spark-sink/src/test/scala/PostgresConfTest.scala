//package io.frama.parisni.spark.postgres
import com.opentable.db.postgres.junit.{EmbeddedPostgresRules, SingleInstancePostgresRule}
import io.frama.parisni.spark.postgres.PGTool

import org.apache.spark.sql.QueryTest
import org.junit.{Rule, Test}

import scala.annotation.meta.getter

class PostgresConfTest extends QueryTest with SparkSessionTestWrapper {

  def verifySpark(query: String): Unit = {
    spark.sql(query).show
  }

  /** create Embedded Postgres Tables **/
  def createPostgresTables() { // Uses JUnit-style assertions
    println(pg.getEmbeddedPostgres.getJdbcUrl("postgres", "pg"))
    val con = pg.getEmbeddedPostgres.getPostgresDatabase.getConnection

    // Create tables source & target
    val req = """
    CREATE TABLE IF NOT EXISTS source (
      id integer NOT NULL,
      pk2 varchar NOT NULL,
      details varchar NULL,
      date_update date NOT NULL default current_date
    );

    CREATE TABLE IF NOT EXISTS target (
      id integer NOT NULL,
      pk2 varchar NOT NULL,
      details varchar NULL,
      date_update date NOT NULL default current_date,
      date_update2 date NULL,
      date_update3 date NULL
    );
    """
    val res2 = con.createStatement().executeUpdate(req)        //("create table test(i int)")

    // Populate table source
    val reqInsertSource = """
    INSERT INTO source(id, pk2, details, date_update)
    VALUES
      (1, 'id1', 'test details of 1st row', '2016-02-01'),
      (2, 'id2', 'test details of 2nd row', '2017-06-05'),
      (3, 'id3', 'test details of 3rd row', '2017-08-07'),
      (4, 'id4', 'test details of 4th row', '2018-10-16'),
      (5, 'id5', 'test details of 5th row', '2019-12-27'),
      (6, 'id6', 'test details of 6th row', '2020-01-14');
    """
    val res3 = con.createStatement().executeUpdate(reqInsertSource)

    val resSource = con.createStatement().executeQuery("select * from source")
    while (resSource.next())
      println(resSource.getInt(1).toString + " " + resSource.getString(2) + " " +
        resSource.getString(3) + " " + resSource.getString(4))

    // Populate table target
    val reqInsertTarget = """
    INSERT INTO target(id, pk2, details, date_update)
    VALUES
      (1, 'id1', 'test details of 1st row', '2019-06-16'),
      (2, 'id2', 'test details of 2nd row', '2018-07-25'),
      (3, 'id3', 'test details of 3rd row', '2020-11-19');
    """
    val res33 = con.createStatement().executeUpdate(reqInsertTarget)

    val resTarget = con.createStatement().executeQuery("select * from target")
    while (resTarget.next())
      println(resTarget.getInt(1).toString + " " + resTarget.getString(2) + " " +
        resTarget.getString(3) + " " + resTarget.getString(4) + " " +
        resTarget.getString(5) + " " + resTarget.getString(6))

  }

  @Test def verifyLoadFromPostgres() {

    println(pg.getEmbeddedPostgres.getJdbcUrl("postgres", "pg"))
    val con = pg.getEmbeddedPostgres.getPostgresDatabase.getConnection

    // create Embedded Postgres Tables
    createPostgresTables()

    val mapy = Map("S_TABLE_NAME" -> "source", "S_TABLE_TYPE" -> "postgres", "S_DATE_FIELD" -> "date_update",
      "T_TABLE_NAME" -> "target", "T_TABLE_TYPE" -> "postgres",  "T_DATE_MAX" -> "2018-10-16", "T_LOAD_TYPE" -> "full",
      "HOST" -> "localhost", "PORT" -> s"${pg.getEmbeddedPostgres.getPort}", "DATABASE" -> "postgres", "USER" -> "postgres",
      "SCHEMA" -> "public"  //,   "T_LOAD_TYPE" -> "scd1", "T_HASH_FIELD" -> "id"
    )
    val dates = List("date_update", "date_update2", "date_update3")
    val pks = List("id","pk2")
    val pgc = new PostgresConf(mapy, dates, pks)

    val host = pgc.getHost.getOrElse("localhost")
    val port = pgc.getPort.getOrElse("5432")
    val db = pgc.getDB.getOrElse("postgres")
    val user = pgc.getUser.getOrElse("postgres")
    val schema = pgc.getSchema.getOrElse("public")
    val s_table = pgc.getSourceTableName.getOrElse("")
    val s_date_field = pgc.getSourceDateField.getOrElse("")
    val date_max = pgc.getDateMax.getOrElse("2019-01-01")
    val t_table = pgc.getTargetTableName.getOrElse("")
    val load_type = pgc.getLoadType.getOrElse("2019-01-01")
    //val hash_field = pks.toString()

    // load table from source
    println(s"Table ${s_table}")
    //verifySpark("select * from source")
    val s_df = pgc.readSource(spark, host, port, db, user, schema, s_table, s_date_field, date_max)
    s_df.show()

    println(f"Table ${t_table} before update")
    //verifySpark("select * from target")
    val targSource = con.createStatement().executeQuery("select * from target")
    while (targSource.next())
      println(targSource.getInt(1).toString + " " + targSource.getString(2) + " " +
        targSource.getString(3) + " " + targSource.getString(4))

    // write table to target
    pgc.writeSource(s_df, host, port, db, user, schema, t_table, load_type) //, hash_field)

    println(f"Table ${t_table} after update")
    //verifySpark("select * from target")
    val targSource2 = con.createStatement().executeQuery("select * from target")
    while (targSource2.next())
      println(targSource2.getInt(1).toString + " " + targSource2.getString(2) + " " +
        targSource2.getString(3) + " " + targSource2.getString(4))

  }
}


import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  // looks like crazy but compatibility issue with junit rule (public)
  @(Rule@getter)
  var pg: SingleInstancePostgresRule = EmbeddedPostgresRules.singleInstance()

  def getPgUrl = pg.getEmbeddedPostgres.getJdbcUrl("postgres", "postgres") + "&currentSchema=public"

  def getPgTool() = PGTool(spark, getPgUrl, "/tmp")

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "1")
      .getOrCreate()
  }

}