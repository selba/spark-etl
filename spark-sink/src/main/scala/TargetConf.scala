

trait TargetConf {

  private val dateFmt = "yyyy-MM-dd"

  val T_TABLE_NAME: String = "T_TABLE_NAME"
  val T_TABLE_TYPE: String = "T_TABLE_TYPE"  //"postgres", "solr", "delta", ....
  val T_DATE_MAX: String = "T_DATE_MAX"      //add function convert to Date data type
  val T_LOAD_TYPE: String = "T_LOAD_TYPE"    //"full", "scd1", "scd2", ...
  //val T_LAST_UPDATE_FIELDS: String = "T_LAST_UPDATE_FIELDS"   // names of fields last_update_date in target table

  def getTargetTableName: Option[String]
  def getTargetTableType: Option[String]
  def getLoadType: Option[String]
  def getDateMax: Option[String]
  //def getLastUpdateFields: Option[List[String]]

  def calculDateMax(t_table: String, date_fields: List[String]): String = { return "" }

  //def calculDateMax(spark: SparkSession, t_table: String, date_fields: List[String], config: Map[String, String]): String = {

    /**
    val query = f"select max(${date_fields}) from ${t_table}"
    println(query.toString)

    val df = spark.read.format("postgres")
      //.option("url", f"jdbc:postgres://${host}:${port}/${db}?user=${user}&currentSchema=public")
      .option("host", host)
      .option("port", port)
      .option("database", db)
      .option("user", user)
      .option("query", query)
      .load

    df1
    **/
    //return ""
  //}

  //def writeSource(s_df: DataFrame, strings: String*): Unit
}
