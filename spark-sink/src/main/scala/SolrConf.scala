class SolrConf (config: Map[String, String], dates: List[String], pks: List[String]) extends TargetConf with SourceConf {

  require(config != null, "Config cannot be null")
  require(config.nonEmpty, "Config cannot be empty")

  // Solr accepts just insert/full as loading type
  /**
  require(config.get(T_LOAD_TYPE).isEmpty || (config.get(T_LOAD_TYPE).isDefined
    && ("full" :: "scd1" :: "scd2" :: Nil).contains(config.get(T_LOAD_TYPE).get)),
    "Loading type shall be in full, scd1, scd2")
  **/

  require(config.get(S_TABLE_TYPE).isEmpty || (config.get(S_TABLE_TYPE).isDefined
    && ("postgres" :: "solr" :: "delta" :: Nil).contains(config.get(S_TABLE_TYPE).get)),
    "Source table shall be in postgres, solr, delta")

  require(config.get(T_TABLE_TYPE).isEmpty || (config.get(T_TABLE_TYPE).isDefined
    && ("postgres" :: "solr" :: "delta" :: Nil).contains(config.get(T_TABLE_TYPE).get)),
    "Target table shall be in postgres, solr, delta")

  // SourceTable methods
  def readSource: Unit = {println("inside readSource function")}
  override def getSourceTableName = config.get(S_TABLE_NAME)
  override def getSourceTableType = config.get(S_TABLE_TYPE)
  override def getSourceDateField = config.get(S_DATE_FIELD)
  def getSourcePK = pks    //config.get(S_PK)

  // TargetTable methods
  override def getTargetTableName = config.get(T_TABLE_NAME)
  override def getTargetTableType = config.get(T_TABLE_TYPE)
  override def getLoadType = config.get(T_LOAD_TYPE)
  def getLastUpdateFields = dates
  //override def getDateMax = config.get(T_DATE_MAX)
  override def getDateMax =
    if (config.get(T_DATE_MAX).isDefined) Some(config(T_DATE_MAX))
    else Some(calculDateMax(config.get(T_TABLE_NAME).toString,pks))

  def writeSource: Unit = {println("inside writeSource function")}
}

object SolrConfTest{
  def main(args: Array[String]): Unit = {

    val mapy = Map("S_TABLE_NAME" -> "table_solr", "S_TABLE_TYPE" -> "solr",
      "T_TABLE_NAME" -> "table_delta", "T_TABLE_TYPE" -> "delta", "T_LOAD_TYPE" -> "full")
    val dates = List("DATE_MAX", "DATE_MAX2", "DATE_MAX3")
    val pks = List("id","pk2")
    val solrc = new SolrConf(mapy, dates, pks)

    println("getSourceTableName = " + solrc.getSourceTableName + " & mapy(\"S_TABLE_NAME\") = " + mapy("S_TABLE_NAME"))
    println("getSourceTableType = " + solrc.getSourceTableType)
    println("getSourcePK = " + solrc.getSourcePK)
    println("getTargetTableName = " + solrc.getTargetTableName)
    println("getTargetTableType = " + solrc.getTargetTableType)
    println("getLoadType = " + solrc.getLoadType)
    println("getDateMax = " + solrc.getDateMax)
    println("getLastUpdateFields = " + solrc.getLastUpdateFields)
    //println(" & " + pgc.getDateMax == Some(mapy("T_DATE_MAX")))
    solrc.readSource
    solrc.writeSource


    //println("Keys in mapy: "+ mapy.keys)
    //println("values in mapy: "+ mapy.values)
  }
}