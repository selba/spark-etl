

class DeltaConf(config: Map[String, String], dates: List[String], pks: List[String]) extends TargetConf with SourceConf {

  require(config != null, "Config cannot be null")
  require(config.nonEmpty, "Config cannot be empty")

  require(config.get(T_LOAD_TYPE).isEmpty || (config.get(T_LOAD_TYPE).isDefined
    && ("full" :: "scd1" :: "scd2" :: Nil).contains(config.get(T_LOAD_TYPE).get)),
    "Loading type shall be in full, scd1, scd2")

  require(config.get(S_TABLE_TYPE).isEmpty || (config.get(S_TABLE_TYPE).isDefined
    && ("postgres" :: "solr" :: "delta" :: Nil).contains(config.get(S_TABLE_TYPE).get)),
    "Source table shall be in postgres, solr, delta")

  require(config.get(T_TABLE_TYPE).isEmpty || (config.get(T_TABLE_TYPE).isDefined
    && ("postgres" :: "solr" :: "delta" :: Nil).contains(config.get(T_TABLE_TYPE).get)),
    "Target table shall be in postgres, solr, delta")

  // SourceTable fields & methods
  val PATH: String = "PATH"     // (PATH is used by Delta for connexion => No PK is needed

  def readSource: Unit = {println("inside readSource function")}
  override def getSourceTableName = config.get(S_TABLE_NAME)
  override def getSourceTableType = config.get(S_TABLE_TYPE)
  override def getSourceDateField = config.get(S_DATE_FIELD)
  def getSourcePK = pks    //.get(S_PK)
  def getPath = config.get(PATH)

  // TargetTable methods
  override def getTargetTableName = config.get(T_TABLE_NAME)
  override def getTargetTableType = config.get(T_TABLE_TYPE)
  override def getLoadType = config.get(T_LOAD_TYPE)
  //override def getDateMax = config.get(T_DATE_MAX)
  override def getDateMax =
    if (config.get(T_DATE_MAX).isDefined) Some(config(T_DATE_MAX))
    else Some(calculDateMax(config.get(T_TABLE_NAME).toString,pks))

  def getLastUpdateFields = dates    //config.get(T_DATE_FIELD_NAME)

  def writeSource: Unit = {println("inside writeSource function")}

}


object DeltaConfTest{
  def main(args: Array[String]): Unit = {

    val mapy = Map("S_TABLE_NAME" -> "table_delta", "S_TABLE_TYPE" -> "delta", "PATH" -> "/root/data",
      "T_TABLE_NAME" -> "table_pg", "T_TABLE_TYPE" -> "postgres",
      "T_DATE_MAX" -> "16/06/2016", "T_LOAD_TYPE" -> "scd1")
    val dates = List("DATE_MAX", "DATE_MAX2", "DATE_MAX3")
    val pks = List("id","pk2")
    val deltac = new DeltaConf(mapy, dates, pks)

    println("getSourceTableName = " + deltac.getSourceTableName + " & mapy(\"S_TABLE_NAME\") = " + mapy("S_TABLE_NAME"))
    println("getSourceTableType = " + deltac.getSourceTableType)
    println("getPath = " + deltac.getPath)
    println("getSourcePK = " + deltac.getSourcePK)
    println("getTargetTableName = " + deltac.getTargetTableName)
    println("getTargetTableType = " + deltac.getTargetTableType)
    println("getLoadType = " + deltac.getLoadType)
    println("getDateMax = " + deltac.getDateMax)
    println("getLastUpdateFields = " + deltac.getLastUpdateFields)
    //println(" & " + deltac.getDateMax == Some(mapy("T_DATE_MAX")))
    //deltac.readSource
    //deltac.writeSource


    //println("Keys in mapy: "+ mapy.keys)
    //println("values in mapy: "+ mapy.values)
  }
}