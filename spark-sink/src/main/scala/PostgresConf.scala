import org.apache.spark.sql.{DataFrame, SparkSession}
//package io.frama.parisni.spark.postgres

class PostgresConf(config: Map[String, String], dates: List[String], pks: List[String])
  extends TargetConf with SourceConf{   // with SparkSessionTestWrapper{

  require(config != null, "Config cannot be null")
  require(config.nonEmpty, "Config cannot be empty")

  require(config.get(T_LOAD_TYPE).isEmpty || (config.get(T_LOAD_TYPE).isDefined
    && ("full" :: "scd1" :: "scd2" :: Nil).contains(config.get(T_LOAD_TYPE).get)),
    "Loading type shall be in full, scd1, scd2")

  require(config.get(S_TABLE_TYPE).isEmpty || (config.get(S_TABLE_TYPE).isDefined
    && ("postgres" :: "solr" :: "delta" :: Nil).contains(config.get(S_TABLE_TYPE).get)),
    "Source table shall be in postgres, solr, delta")

  require(config.get(T_TABLE_TYPE).isEmpty || (config.get(T_TABLE_TYPE).isDefined
    && ("postgres" :: "solr" :: "delta" :: Nil).contains(config.get(T_TABLE_TYPE).get)),
    "Target table shall be in postgres, solr, delta")

  //val URL: String = "URL"
  val HOST: String = "HOST"
  val PORT: String = "PORT"
  val DATABASE: String = "DATABASE"
  val USER: String = "USER"
  val SCHEMA: String = "SCHEMA"

  def getHost: Option[String] = config.get(HOST)
  def getPort: Option[String] = config.get(PORT)
  def getDB: Option[String] = config.get(DATABASE)
  def getUser: Option[String] = config.get(USER)
  def getSchema: Option[String] = config.get(SCHEMA)


  // "jdbc:postgresql://%s:%s/%s?user=%s&currentSchema=%s"
  //  .format(host.get, port.getOrElse(5432), database.get, user.get, schema.getOrElse("public"))

  // SourceTable methods
  /**
  override def readSource(spark: SparkSession, host: Option[String], port: Option[String],
                 db: Option[String], user: Option[String], schema: Option[String],
                 s_table: Option[String], s_date_field: Option[String],
                 date_Max: Option[String]): DataFrame = {
  **/
  def readSource(spark: SparkSession, host: String, port: String,
                          db: String, user: String, schema: String,
                          s_table: String, s_date_field: String,
                          date_Max: String): DataFrame = {
    println("Loading table from postgres")

    /**
    val input = spark.sql("select 1 as t")
    input
      .write.format("postgres")
      .option("host", "localhost")
      .option("port", pg.getEmbeddedPostgres.getPort)
      .option("database", "postgres")
      .option("user", "postgres")
      .option("table", "test_table")
      .mode(org.apache.spark.sql.SaveMode.Overwrite)
      .save
     **/

    val query = f"select * from ${s_table} where ${s_date_field} >= '${date_Max}'::date"
    println(query.toString)

    val df1 = spark.read.format("postgres")
      //.option("url", f"jdbc:postgres://${host}:${port}/${db}?user=${user}&currentSchema=public")
      .option("host", host)
      .option("port", port)
      .option("database", db)
      .option("user", user)
      .option("query", query)
      .option("partitions",4)
      .option("partitionColumn","id")
      //.option("numSplits",5)
      .load

     df1
  }

  override def getSourceTableName = config.get(S_TABLE_NAME)
  override def getSourceTableType = config.get(S_TABLE_TYPE)
  override def getSourceDateField = config.get(S_DATE_FIELD)
  def getSourcePK = pks    //config.get(S_PK)

  // TargetTable methods
  override def getTargetTableName = config.get(T_TABLE_NAME)
  override def getTargetTableType = config.get(T_TABLE_TYPE)
  override def getLoadType = config.get(T_LOAD_TYPE)
  override def getDateMax =
    if (config.get(T_DATE_MAX).isDefined) Some(config(T_DATE_MAX))
    else Some(calculDateMax(config.get(T_TABLE_NAME).toString,pks))

  def getLastUpdateFields = dates  //.get(T_DATE_FIELD_NAME)

  //override def writeSource: Unit = {println("inside writeSource function")}
  def writeSource(s_df: DataFrame, host: String, port: String,
                 db: String, user: String, schema: String,
                 t_table: String, load_type: String): Unit = {    //hash_field: String

    println(f"Synchronizing table source with table ${t_table}")
    s_df.write.format("postgres")
      .option("type",load_type)
      .option("partitions",4)
      .option("host", host)
      .option("port", port)
      .option("database", db)
      .option("user", user)
      .option("table", t_table)
      .mode(org.apache.spark.sql.SaveMode.Overwrite)
      .save

    /**
    load_type match{
      case "full" => {
        s_df.write.format("postgres")
        .option("type",load_type)
        .option("partitions",4)
        .option("host", host)
        .option("port", port)
        .option("database", db)
        .option("user", user)
        .option("table", t_table)
        .mode(org.apache.spark.sql.SaveMode.Overwrite)
        .save
      }
      case "scd1" => {
      s_df.write.format ("postgres")
      .option ("type", load_type)
      .option ("JoinKey", hash_field)
      .option ("partitions", 4)
      .option ("host", host)
      .option ("port", port)
      .option ("database", db)
      .option ("user", user)
      .option ("table", t_table)
      .mode (org.apache.spark.sql.SaveMode.Overwrite)
      .save
     }
     }
  **/

  }

}

/**
object PostgresConfTest{
  def main(args: Array[String]): Unit = {

    val mapy = Map("S_TABLE_NAME" -> "table_postgres", "S_TABLE_TYPE" -> "postgres",
      "T_TABLE_NAME" -> "table_delta", "T_TABLE_TYPE" -> "delta",
      "T_DATE_MAX" -> "16/06/2016", "T_LOAD_TYPE" -> "full")
    val dates = List("DATE_MAX", "DATE_MAX2", "DATE_MAX3")
    val pks = List("id","pk2")
    var pgc = new PostgresConf(mapy, dates, pks)

    println("getSourceTableName = " + pgc.getSourceTableName + " & mapy(\"S_TABLE_NAME\") = " + mapy("S_TABLE_NAME"))
    println("getSourceTableType = " + pgc.getSourceTableType)
    println("getSourcePK = " + pgc.getSourcePK)
    println("getTargetTableName = " + pgc.getTargetTableName)
    println("getTargetTableType = " + pgc.getTargetTableType)
    println("getLoadType = " + pgc.getLoadType)
    println("getDateMax = " + pgc.getDateMax)
    println("getLastUpdateFields = " + pgc.getLastUpdateFields)
    //println(" & " + pgc.getDateMax == Some(mapy("T_DATE_MAX")))
    pgc.readSource
    pgc.writeSource

    //println("Keys in mapy: "+ mapy.keys)
    //println("values in mapy: "+ mapy.values)

  }
}
**/
